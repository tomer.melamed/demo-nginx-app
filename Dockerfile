# Use the official Nginx base image
FROM --platform=linux/arm64 nginx

# Remove default Nginx index.html
RUN rm -rf /usr/share/nginx/html/*

# Copy your custom index.html to the Nginx folder
COPY index.html /usr/share/nginx/html/

# Expose port 80 for Nginx
EXPOSE 80